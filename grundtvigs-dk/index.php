<?php get_header(); ?>

<div class="container-fluid" id="content">


    <div class="container">
        <div class="col-12">

            <div class="row">
                <div class="col-12 bg-tertiary padbox topbox">
                    <h1>Nyt fra Grundtvigs</h1>
                    <h6>På Grundtvigs Højskole har vi startet vores egen redaktion. Den hedder Grund. Her vil vi gennem billede, tekst og lyd dokumentere livet på højskole.</h6>
                </div>
            </div>

            <div class="col-12 nopad">
                <?php $index = 0; ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php
                    if($index % 2) {
                        $paragraph  = ' order-2 order-md-1';
                        $image      = ' order-1 order-md-2';
                    } else {
                        $paragraph  = ' order-2 order-md-2';
                        $image      = ' order-1 order-md-1';
                    }
                    $image_src = get_the_post_thumbnail_url(get_the_ID(), 'medium-large');
                ?>
                    <div class="row row-eq height nyhed">
                        <div class="col-12 col-md-6 content <?php echo $paragraph; ?>">
                            <p>
                                <a href="<?php the_permalink(); ?>" class="btn btn-grundtvigs blue text-primary btn-block text-left iconbg-tertiary icon-hand-o-right">
                                    <span><?php the_title() ?></span>
                                </a>
                            </p>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                        <a href="<?php the_permalink(); ?>">
                        <div class="col-12 col-md-6 bg-secondary image img-center img-cover<?php echo $image; ?>" style="background-image:url(<?php echo $image_src; ?>)">
                        </a>
                        </div>
                    </div>
                <?php $index++; ?>
                <?php endwhile; endif; ?>

            </div>

        </div>
    </div>

</div>

<?php get_footer(); ?>
