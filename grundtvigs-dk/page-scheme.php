<?php
/*
Template Name: Skema
*/
?>
<?php get_header(); ?>

<div class="container-fluid" id="content">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="container">
        <div class="col-12 bg-tertiary padbox topbox">
            <h1><?php the_title(); ?></h1>
            <h6><?php the_content(); ?></h6>
        </div>
    </div>

    <?php endwhile; endif; ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>
                    <?php $how_to = get_field_object('how_to'); ?>
                    <a id="how_to_toggle" data-toggle="collapse" href="#sadan_gor_du" aria-expanded="false" aria-controls="sadan_gor_du">
                        <i class="fa fa-hand-o-right"></i>
                        <span><?php echo $how_to['label']; ?></span>
                    </a>
                </p>
                <div class="collapse" id="sadan_gor_du">
                    <?php the_field('how_to'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 modul_valg">
                <a id="modul_1" href="#/" class="btn btn-secondary btn-lg mr-3 active">Periode 1</a>
                <a id="modul_2" href="#/" class="btn btn-secondary btn-lg">Periode 2</a>
            </div>
        </div>
    </div>



    <div class="container">
        <div class="row">
                <?php echo get_template_part('library/parts/skema'); ?>
        </div>
    </div>

    <div class="container">

            <div class="row row-eq-height img_and_content">

                    <div class="col-12 col-md-6 bg-primary padbox order-2 order-md-2 content">
                        <?php echo the_field('price_box_0'); ?>
                    </div>
                    <?php $image = get_field('price_box_0_image'); ?>
                    <div class="col-12 col-md-6 padbox img-center img-cover order-1 order-md-1 image" style="min-height: 300px;background-image:url(<?php echo $image['sizes']['large']; ?>);">
                    </div>
            </div>

            <div class="row row-eq-height prices">
                <div class="col-12 col-md-6 padbox bg-primary">
                    <?php the_field('price_box_1'); ?>
                </div>
                <div class="col-12 col-md-6 padbox bg-secondary text-primary">
                    <?php the_field('price_box_2'); ?>
                </div>
            </div>

            <div class="row row-eq-height">
                <div class="col-12 col-md-3 padbox text-center bg-secondary text-primary">
                    <p>Tillæg for værelse med bad og toilet</p>
                    <p class="lead"><?php the_field('price_box_4'); ?></p>
                    <p>kr. pr. uge</p>
                </div>
                <div class="col-12 col-md-3 padbox text-center bg-tertiary text-secondary">
                    <p>Tillæg for eneværelse</p>
                    <p class="lead"><?php the_field('price_box_5'); ?></p>
                    <p>kr. pr. uge</p>
                </div>
                <div class="col-12 col-md-6 bg-primary padbox">
                    <?php the_field('price_box_3'); ?>
                </div>
            </div>
    </div>

    <div class="container">

            <?php echo get_template_part('library/parts/page','builder'); ?>

    </div>


    <div id="modal" class="modal fade">

        <div class="modal-dialog">

            <div id="modal_target" class="modal-content"></div>

        </div>

    </div>

</div>

<?php get_footer(); ?>
