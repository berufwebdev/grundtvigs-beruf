    <div class="col-12 col-lg-9 order-lg-1 order-2">
                <table id="skema" class="table table-responsive grundtvigs module_1">
                    <caption><em>NB: Ret til ændringer forbeholdes</em></caption>
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">mandag</th>
                            <th scope="col">tirsdag</th>
                            <th scope="col">onsdag</th>
                            <th scope="col">torsdag</th>
                            <th scope="col">fredag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">8.00</th>
                            <td colspan="5" class="bg-tertiary text-secondary">Morgenmad</td>
                        </tr>
                        <tr>
                            <th scope="row">8.40</th>
                            <td colspan="5" class="bg-secondary text-primary">Morgensamling</td>
                        </tr>
                        <tr>
                            <th scope="row">9.00</th>
                            <td rowspan="3" class="time block_a">
                                <span class="default">A</span>
                            </td>
                            <td class="time block_c">
                                <span class="module_1 default">C</span>
                                <span class="module_2 default">C</span>
                            </td>
                            <td rowspan="2" class="time block_a">
                                <span class="default">A</span>
                            </td>
                            <td class="time block_b">
                                <span class="module_1 default">B</span>
                                <span class="module_2 default">B</span>
                            </td>
                            <td class="time block_c">
                                <span class="module_1 default">C</span>
                                <span class="module_2 default">C</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">11.00</th>
                            <td rowspan="2" class="bg-primary text-secondary">Ganggruppemøde</td>
                            <td rowspan="2">Torsdagstimen</td>
                            <td rowspan="2" class="fri"></td>
                        </tr>
                        <tr>
                            <th scope="row">11.45</th>
                            <td class="fri"></td>
                        </tr>
                        <tr>
                            <th scope="row">12.00</th>
                            <td colspan="5" class="bg-tertiary text-secondary">Middag</td>
                        </tr>
                        <tr>
                            <th scope="row">13.00</th>
                            <td colspan="3" class="fri"></td>
                            <td class="bg-primary text-secondary">Fællesmøde</td>
                            <td class="fri"></td>
                        </tr>
                        <tr>
                            <th scope="row">14.00</th>
                            <td class="time block_b">
                                <span class="module_1 default">B</span>
                                <span class="module_2 default">B</span>
                            </td>
                            <td class="time block_d">
                                <span class="module_1 default">D</span>
                                <span class="module_2 default">D</span>
                            </td>
                            <td class="time block_a">
                                <span class="default">A</span>
                            </td>
                            <td class="time block_d">
                                <span class="module_1 default">D</span>
                                <span class="module_2 default">D</span>
                            </td>
                            <td class="time block_b">
                                <span class="module_1 default">B</span>
                                <span class="module_2 default">B</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">16.00</th>
                            <td class="fri"></td>
                            <td>Sportstilbud</td>
                            <td class="fri"></td>
                            <td>Sportstilbud</td>
                            <td class="fri"></td>
                        </tr>
                        <tr>
                            <th scope="row">18.00</th>
                            <td colspan="5" class="bg-tertiary text-secondary">Aftensmad</td>
                        </tr>
                        <tr>
                            <th scope="row">19.30</th>
                            <td class="time block_e">
                                <span class="module_1 default">E</span>
                                <span class="module_2 default">E</span>
                            </td>
                            <td>Levende ord</td>
                            <td class="fri"></td>
                            <td>Kulturcafe</td>
                            <td class="fri"></td>
                        </tr>
                    </tbody>
                </table>
        </div>

        <div class="col-12 col-lg-3 order-lg-2 order-1 mb-md-5">

            <div id="courses" data-children=".item" class="module_1">
                <h3><span id="course_selector">Periode 1</span></h3>
                <div class="item">
                    <?php if( have_rows('a_courses') ): ?>
                    <a data-toggle="collapse" data-parent="#courses" href="#a_courses" aria-expanded="true" aria-controls="a_courses">
                        <h2>A-fag</h2>
                    </a>
                    <div id="a_courses" class="collapse show" role="tabpanel">

                        <ul>
                        <?php
                        while ( have_rows('a_courses') ) : the_row();
                            $fag = get_sub_field('course_name');
                        ?>
                            <li>
                                <a href="#/" class="courseinfo" data-id="<?php echo $fag->ID; ?>"><?php echo $fag->post_title; ?></a>
                                <a href="#/" class="choosecourse" data-block="block_a" data-text="<?php echo $fag->post_title; ?>" data-id="<?php echo $fag->ID; ?>"><i class="fa fa-plus pr-2 pull-right"></i></a>
                            </li>
                        <?php endwhile; ?>
                        </ul>

                    </div>
                    <?php endif; ?>
                </div>

              <?php
              $fag_typer = array('a','b','c','d','e');

              for ($i=1; $i < 3; $i++) :

                  $module_class = 'module_' . $i;

                  for ($j=1; $j < 5; $j++) :

                      $collapseid   = $i . '_' . $fag_typer[$j] . '_courses';
                      $rep_object   = get_field_object($collapseid);
                      ?>
                      <div class="item <?php echo $module_class ?>">
                         <?php if( have_rows($collapseid) ): ?>
                            <a data-toggle="collapse" data-parent="#courses" href="#<?php echo $collapseid; ?>" aria-expanded="false" aria-controls="<?php echo $collapseid; ?>">
                                <h2><?php echo $rep_object['label']; ?></h2>
                            </a>
                            <div id="<?php echo $collapseid; ?>" class="collapse" role="tabpanel">
                              <ul>
                                  <?php while ( have_rows($collapseid) ) : the_row();
                                      $fag = get_sub_field('course_name');
                                  ?>
                                      <li>
                                          <a href="#/" class="courseinfo" data-id="<?php echo $fag->ID; ?>"><?php echo $fag->post_title; ?></a>
                                          <a href="#/" class="choosecourse" data-module="module_<?php echo $i; ?>" data-block="block_<?php echo $fag_typer[$j]; ?>" data-text="<?php echo $fag->post_title; ?>" data-id="<?php echo $fag->ID; ?>"><i class="fa fa-plus pr-2 pull-right"></i></a>
                                      </li>
                                  <?php endwhile; ?>
                              </ul>
                            </div>
                        <?php endif; ?>
                      </div>
                  <?php endfor; ?>

            <?php endfor; ?>
            <a id="reset" href="#/" class="mt-3 btn-grundtvigs red text-secondary text-center icon-ban iconbg-secondary btn-block">
                <span>Nulstil</span>
            </a>

            </div>
