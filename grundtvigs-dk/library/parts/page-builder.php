<?php

if( have_rows('sections') ):
    $row_no = 1;
    ?>
    <div class="pagebuilder">
    <?php
    while ( have_rows('sections') ) : the_row();

        $condition     = $row_no % 2;
        // single eller archive side.
        if( is_singular('sommerkursus') || (get_the_ID() == 156) ) {
            $condition = $condition;
        } else {
            // starter omvendt
            $condition = !$condition;
        }
        if($condition) {
            $contentbox = ' order-2 order-md-2 content';
            $imagebox   = ' order-1 order-md-1';
        } else {
            $contentbox = ' order-2 order-md-1 content';
            $imagebox   = ' order-1 order-md-2';
        }

        if( get_row_layout() == 'content_and_image' ):

        	$content       = get_sub_field('content');
            $image         = get_sub_field('image');

            ?>

            <div class="row row-eq-height img_and_content">

                    <div class="col-12 col-md-6 bg-primary padbox<?php echo $contentbox; ?>">
                        <?php echo $content; ?>
                    </div>
                    <div class="col-12 col-md-6 padbox img-center img-cover<?php echo $imagebox; ?>" style="background-image:url(<?php echo $image['sizes']['large']; ?>);">

                    </div>
            </div>

            <?php

        elseif( get_row_layout() == 'sommerkurser' ):

            $content       = get_sub_field('uge');
            $image         = get_sub_field('image');
            $kurser        = get_sub_field('kurser');

            ?>
            <div class="row row-eq-height img_and_content">

                    <div class="col-12 col-md-6 bg-primary padbox<?php echo $contentbox; ?>">
                        <h3><?php echo $content; ?></h3>
                        <?php if($kurser) : foreach($kurser as $kursus): ?>
                            <p>
                                <a href="<?php the_permalink($kursus->ID) ?>" class="btn-grundtvigs btn-block blue text-primary icon-hand-o-right iconbg-tertiary">
                                    <span><?php echo get_the_title($kursus->ID); ?></span>
                                </a>
                            </p>
                        <?php endforeach; endif; ?>
                    </div>
                    <div class="col-12 col-md-6 bg-secondary padbox img-center img-cover<?php echo $imagebox; ?>" style="background-image:url(<?php echo $image['sizes']['large']; ?>);">

                    </div>
            </div>

            <?php

        elseif( get_row_layout() == '2_col_content' ):

            $content_1      = get_sub_field('content_1');
            $content_2      = get_sub_field('content_2');

            ?>

            <div class="row row-eq-height 2_col_content">

                    <div class="col-12 col-md-6 padbox<?php echo $contentbox; ?>">
                        <?php echo $content_1; ?>
                    </div>
                    <div class="col-12 col-md-6 padbox<?php echo $contentbox; ?>">
                        <?php echo $content_2; ?>
                    </div>
            </div>

            <?php

        elseif( get_row_layout() == 'content_full_width' ):

            $content      = get_sub_field('content');
            $row_no = $row_no - 1;

            ?>

            <div class="row row-eq-height content_full_width">

                    <div class="col-12 padbox content">
                        <?php echo $content; ?>
                    </div>

            </div>

            <?php

        elseif( get_row_layout() == 'content_video' ):

            $iframe      = get_sub_field('video_src');
            preg_match('/src="(.+?)"/', $iframe, $matches);
            $src = $matches[1];
            $params = array(
                'controls'      => 1,
                'hd'            => 1,
                'autohide'      => 1,
                'rel'           => 0,
            );
            $new_src = add_query_arg($params, $src);
            $iframe = str_replace($src, $new_src, $iframe);
            $attributes = 'frameborder="0"';
            $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
            $row_no = $row_no - 1;

            ?>

            <div class="row row-eq-height content_video">

                    <div class="col-12 nopad embed-container mt-3 mb-3">
                        <?php echo $iframe; ?>
                    </div>

            </div>

            <?php


        elseif( get_row_layout() == 'priser' ):

            $prices      = 'prices';

            ?>

            <div class="row row-eq-height prices">
                <?php if( have_rows($prices) ) : while( have_rows($prices) ) : the_row();

                    $type = get_sub_field('content_type');

                    if($type == 'price') :

                        $title = get_sub_field('pris_title');
                        $price = get_sub_field('pris_amount');

                    ?>

                        <div class="col-12 col-md-4 padbox content priceblock text-center">
                            <p class="title"><?php echo $title; ?></p>
                            <p class="price"><?php echo $price; ?></p>
                            <p class="btm">kr. pr. person</p>
                        </div>

                    <?php
                    else :
                        $content = get_sub_field('indhold');
                    ?>

                        <div class="col-12 col-md-4 padbox priceblock">
                            <?php echo $content; ?>
                        </div>

                    <?php endif; ?>
                <?php endwhile; endif; ?>
            </div>

        <?php
        elseif( get_row_layout() == 'priser_skema') :

        ?>
        <div class="container">

                <div class="row row-eq-height img_and_content">

                        <div class="col-12 col-md-6 bg-primary padbox order-2 order-md-2 content">
                            <?php echo the_sub_field('price_box_0'); ?>
                        </div>
                        <?php $image = get_sub_field('price_box_0_image'); ?>
                        <div class="col-12 col-md-6 padbox img-center img-cover order-1 order-md-1 image" style="min-height: 300px;background-image:url(<?php echo $image['sizes']['large']; ?>);">
                        </div>
                </div>

                <div class="row row-eq-height prices">
                    <div class="col-12 col-md-6 padbox bg-primary">
                        <?php the_sub_field('price_box_1'); ?>
                    </div>
                    <div class="col-12 col-md-6 padbox bg-secondary text-primary">
                        <?php the_sub_field('price_box_2'); ?>
                    </div>
                </div>

                <div class="row row-eq-height">
                    <div class="col-12 col-md-3 padbox text-center bg-secondary text-primary">
                        <p>Tillæg for værelse med bad og toilet</p>
                        <p class="lead"><?php the_sub_field('price_box_4'); ?></p>
                        <p>kr. pr. uge</p>
                    </div>
                    <div class="col-12 col-md-3 padbox text-center bg-tertiary text-secondary">
                        <p>Tillæg for eneværelse</p>
                        <p class="lead"><?php the_sub_field('price_box_5'); ?></p>
                        <p>kr. pr. uge</p>
                    </div>
                    <div class="col-12 col-md-6 bg-primary padbox">
                        <?php the_sub_field('price_box_3'); ?>
                    </div>
                </div>
        </div>
        <?php
        endif;

    $row_no++;
    endwhile;
    ?>
    </div>
    <?php

else :

    //print_r('no rows found');

endif;

?>
