jQuery(document).ready(function($) {

        var $button = $('.courseinfo');
        var $modal = $('#modal');
        var $modal_target = $('#modal_target');

        $button.click(function() {

            $modal_target.html('<h1><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></h1>');
            $modal.modal('show');

            var id = $(this).data('id');
            //console.log(id);

            $.ajax({
                url: grundtvigs.ajaxurl,
                data: {
                    'action' : 'fetch_modal_content',
                    'id' : id
                },
                success:function(data) {
                    $modal_target.html(data);
                }
            });

        });

});
