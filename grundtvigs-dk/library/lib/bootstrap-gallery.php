<?php
function bootstrap_gallery( $output = '', $atts, $instance )
{
    if (strlen($atts['columns']) < 1) {
        $columns = 3;
    }
    else {
        $columns = $atts['columns'];
    }

    $images = explode(',', $atts['ids']);
    if ($columns < 1 || $columns > 12) {
        $columns == 3;
    }

    $col_class = 'nopad col-4 col-md-' . 12/$columns;

    $return = '<div class="row gallery grundtvigs_gallery">';
    $i = 0;
    foreach ($images as $key => $value) {
        if ($i%$columns == 0 && $i > 0) {
            //$return .= '</div><div class="row gallery grundtvigs_gallery">';
        }
        $image_attributes = wp_get_attachment_image_src($value, 'full');
        $thumbnail_attributes = wp_get_attachment_image_src($value, 'thumbnail');
        $return .= '
            <div class="'.$col_class.' grundtvigs_gallery_col">
                <div class="gallery-image-wrap bg-secondary">
                    <a data-gallery="gallery" href="#/" data-lity data-lity-target="'.$image_attributes[0].'">
                        <img src="'.$thumbnail_attributes[0].'" alt="" class="img-fluid mx-auto d-block" style="width:100%;height:100%;">
                    </a>
                </div>
            </div>';
        $i++;
    }
    $return .= '</div>';
    return $return;
}
add_filter( 'post_gallery', 'bootstrap_gallery', 10, 4);
?>
