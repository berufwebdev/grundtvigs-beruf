<?php
// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

/*****

*/
function grundtvigs_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Nyheder';
    $submenu['edit.php'][5][0] = 'Nyheder';
    $submenu['edit.php'][10][0] = 'Tilføj Nyhed';
	$submenu['edit.php'][15][0] = 'Kategorier';
    $submenu['edit.php'][16][0] = 'Tags';
}
function grundtvigs_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Nyheder';
    $labels->singular_name = 'Nyheder';
    $labels->add_new = 'Tilføj Nyhed';
    $labels->add_new_item = 'Tilføj Nyhed';
    $labels->edit_item = 'Rediger Nyhed';
    $labels->new_item = 'Nyhed';
    $labels->view_item = 'Se Nyhed';
    $labels->search_items = 'Søg Nyheder';
    $labels->not_found = 'Ingen Nyheder fundet';
    $labels->not_found_in_trash = 'Ingen Nyheder fundet i papirkurven.';
    $labels->all_items = 'Alle Nyheder';
    $labels->menu_name = 'Nyheder';
    $labels->name_admin_bar = 'Nyheder';
}

add_action( 'admin_menu', 'grundtvigs_change_post_label' );
add_action( 'init', 'grundtvigs_change_post_object' );

/*********************************
			SOMMERKURSER
**********************************/
function custom_post_sommerkurser() {
	$singular 	= 'Sommerkursus';
	$plural		= 'Sommerkurser';
	$plural_l 	= strtolower($plural);
	$singular_l	= strtolower($singular);

	register_post_type( 'sommerkursus',

		array(
			'labels' => array(
				'name' 					=> __( $plural, 'bonestheme' ),
				'singular_name' 		=> __( $singular, 'bonestheme' ),
				'all_items' 			=> __( 'Alle ' . $plural, 'bonestheme' ),
				'add_new' 				=> __( 'Tilføj nyt', 'bonestheme' ),
				'add_new_item' 			=> __( 'Tilføj nyt ' . $singular, 'bonestheme' ),
				'edit' 					=> __( 'Rediger', 'bonestheme' ),
				'edit_item' 			=> __( 'Rediger ' . $singular, 'bonestheme' ),
				'new_item' 				=> __( 'Nyt ' . $singular, 'bonestheme' ),
				'view_item' 			=> __( 'Se ' . $singular, 'bonestheme' ),
				'search_items' 			=> __( 'Søg i ' . $plural, 'bonestheme' ),
				'not_found' 			=> __( 'Intet fundet i databasen.', 'bonestheme' ),
				'not_found_in_trash' 	=> __( 'Intet fundet i papirkurven', 'bonestheme' ),
				'parent_item_colon' 	=> ''
			), /* end of arrays */
			'description' 			=> __( 'Dette er et eksempel på et ' . $singular, 'bonestheme' ),
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search' 	=> false,
			'show_ui' 				=> true,
			'show_in_rest'			=> true,
			'query_var' 			=> true,
			'menu_position' 		=> 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 			=> 'dashicons-screenoptions', /* the icon for the custom post type menu */
			'rewrite'				=> array( 'slug' => $singular_l, 'with_front' => false ), /* you can specify its url slug */
			'has_archive' 			=> $plural_l, /* you can rename the slug here */
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks')
	 	)
	);

}

/*********************************
			Fag
**********************************/
function custom_post_fag() {
	$singular 	= 'Fag';
	$plural		= 'Fag';
	$plural_l 	= strtolower($plural);
	$singular_l	= strtolower($singular);

	register_post_type( 'fag',

		array(
			'labels' => array(
				'name' 					=> __( $plural, 'bonestheme' ),
				'singular_name' 		=> __( $singular, 'bonestheme' ),
				'all_items' 			=> __( 'Alle ' . $plural, 'bonestheme' ),
				'add_new' 				=> __( 'Tilføj nyt', 'bonestheme' ),
				'add_new_item' 			=> __( 'Tilføj nyt ' . $singular, 'bonestheme' ),
				'edit' 					=> __( 'Rediger', 'bonestheme' ),
				'edit_item' 			=> __( 'Rediger ' . $plural, 'bonestheme' ),
				'new_item' 				=> __( 'Nyt ' . $singular, 'bonestheme' ),
				'view_item' 			=> __( 'Se ' . $singular, 'bonestheme' ),
				'search_items' 			=> __( 'Søg i ' . $plural, 'bonestheme' ),
				'not_found' 			=> __( 'Intet fundet i databasen.', 'bonestheme' ),
				'not_found_in_trash' 	=> __( 'Intet fundet i papirkurven', 'bonestheme' ),
				'parent_item_colon' 	=> ''
			), /* end of arrays */
			'description' 			=> __( 'Dette er et eksempel på et ' . $singular, 'bonestheme' ),
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search' 	=> true,
			'show_ui' 				=> true,
			'show_in_rest'			=> true,
			'query_var' 			=> true,
			'menu_position' 		=> 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 			=> 'dashicons-welcome-widgets-menus', /* the icon for the custom post type menu */
			'rewrite'				=> array( 'slug' => $singular_l, 'with_front' => false ), /* you can specify its url slug */
			'has_archive' 			=> $plural_l, /* you can rename the slug here */
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
	 	)
	);

}

/*********************************
	KALENDER / BEGIVENHED
**********************************/
function custom_post_begivenhed() {
	$singular 	= 'Begivenhed';
	$plural		= 'Begivenheder';
	$plural_l 	= strtolower($plural);
	$singular_l	= strtolower($singular);

	register_post_type( 'begivenhed',

		array(
			'labels' => array(
				'name' 					=> __( $plural, 'bonestheme' ),
				'singular_name' 		=> __( $singular, 'bonestheme' ),
				'all_items' 			=> __( 'Alle ' . $plural, 'bonestheme' ),
				'add_new' 				=> __( 'Tilføj nyt', 'bonestheme' ),
				'add_new_item' 			=> __( 'Tilføj nyt ' . $singular, 'bonestheme' ),
				'edit' 					=> __( 'Rediger', 'bonestheme' ),
				'edit_item' 			=> __( 'Rediger ' . $plural, 'bonestheme' ),
				'new_item' 				=> __( 'Nyt ' . $singular, 'bonestheme' ),
				'view_item' 			=> __( 'Se ' . $singular, 'bonestheme' ),
				'search_items' 			=> __( 'Søg i ' . $plural, 'bonestheme' ),
				'not_found' 			=> __( 'Intet fundet i databasen.', 'bonestheme' ),
				'not_found_in_trash' 	=> __( 'Intet fundet i papirkurven', 'bonestheme' ),
				'parent_item_colon' 	=> ''
			), /* end of arrays */
			'description' 			=> __( 'Dette er et eksempel på et ' . $singular, 'bonestheme' ),
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search' 	=> false,
			'show_ui' 				=> true,
			'show_in_rest'			=> true,
			'query_var' 			=> true,
			'menu_position' 		=> 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 			=> 'dashicons-calendar-alt', /* the icon for the custom post type menu */
			'rewrite'				=> array( 'slug' => $singular_l, 'with_front' => false ), /* you can specify its url slug */
			'has_archive' 			=> 'kalender', /* you can rename the slug here */
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'revisions', 'page-attributes')
	 	)
	);

}
/*
	Sorter begivnheder ved dato ACF field, og hvis kun hvis de er frem i tiden.
*/
function grundtvigs_change_cpt_begivenhed_order( $query )
{
    // validate
    if( is_admin() )
    {
        return $query;
    }

	$queried_object = get_queried_object();
    if( is_post_type_archive('begivenhed') && isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'begivenhed' )
    {
		$today = date('Ymd');
        $query->set('orderby', 'dato');
        $query->set('order', 'ASC');
		$query->set(
			'meta_query',
				array(
					array(
						'key' 		=> 'dato',
						'compare'	=> '>=',
						'value' 	=> $today,
					)
				)
			);
    }
	elseif( is_page_template('page-kalender-arkiv.php') && isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'begivenhed' )
	{
		$today = date('Ymd');
		$query->set('orderby', 'dato');
		$query->set('order', 'DESC');
		$query->set(
			'meta_query',
				array(
					array(
						'key' 		=> 'dato',
						'compare'	=> '<',
						'value' 	=> $today,
					)
				)
			);
	} elseif(is_post_type_archive('fag') && isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'fag' ) {
		$query->set('orderby', 'post_title');
		$query->set('order', 'ASC');
	}

    return $query; // always return

}
add_action('pre_get_posts', 'grundtvigs_change_cpt_begivenhed_order');

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_sommerkurser');
add_action( 'init', 'custom_post_begivenhed');
add_action( 'init', 'custom_post_fag');

?>
