<?php



// function my_acf_options_page_settings( $settings )
// {
// 	$settings['title'] = 'Global';
// 	$settings['pages'] = array('Header', 'Sidebar', 'Footer');

// 	return $settings;
// }

// add_filter('acf/options_page/settings', 'my_acf_options_page_settings');


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Grundtvigs Tema Indstillinger',
		'menu_title'	=> 'Grundtvigs',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'		=> 2,
		'icon_url' 		=> 'dashicons-wordpress-alt'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Cookie Notice Settings',
	// 	'menu_title'	=> 'Cookie',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Kontaktoplysninger',
		'menu_title'	=> 'Kontakt',
		'parent_slug'	=> 'theme-general-settings',
	));
}

?>
