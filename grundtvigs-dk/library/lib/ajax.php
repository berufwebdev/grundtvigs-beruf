<?php
/*************** AJAX ACTION **************/
function fetch_modal_content() {
  if ( isset($_REQUEST) ) {
    $post_id = $_REQUEST['id'];
    $post_object = get_post($post_id);
    $post_content = apply_filters('the_content', $post_object->post_content);
  ?>

    <div class="modal-header">
        <h3 class="h1">
            <a target="_blank" href="<?php echo get_permalink($post_id); ?>">
                <?php echo $post_object->post_title; ?>&nbsp;
                <i class="fa fa-external-link-square"></i>
            </a>
        </h3>

        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
            <i class="fa fa-3x fa-close"></i>
        </button>
    </div>

    <div class="modal-body">
      <?php echo wpautop( $post_content ); ?>
    </div>

  <?php
  }
  die();
}
add_action( 'wp_ajax_fetch_modal_content', 'fetch_modal_content' );
add_action( 'wp_ajax_nopriv_fetch_modal_content', 'fetch_modal_content' );


?>
