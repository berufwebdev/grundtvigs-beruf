<?php
/**
 * Shortcode to generate grundtvigs styled buttons with icons
 * @param [array] $atts User inputs for the shortcode.
 * @return [string] $output_string
 */
function beruf_add_buttons_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'text'   => 'Knaptekst',
            'target' => '_self',
            'url'    => '#',
            'bg'     => 'blue',
			'icon'   => 'icon-hand-o-right',
			'color'  => 'text-primary',
            'iconbg' => 'iconbg-tertiary',
		),
		$atts,
		'knap'
	);

    // Here we collect and set the button attributes
    $btn_class_array = array(
        'btn-grundtvigs',
        $atts['bg'],
        $atts['color'],
        $atts['icon'],
        $atts['iconbg']
    );
    $btn_class = '';
    foreach ($btn_class_array as $class) {
        $btn_class .= $class . ' ';
    }

    // Here we start rendering the button
    ob_start();
    ?>

    <a class="<?php echo $btn_class; ?>" href="<?php echo $atts['url']; ?>" target="<?php echo $atts['target']; ?>">
        <span><?php echo $atts['text']; ?></span>
    </a>

    <?php
    $output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}
add_shortcode( 'knap', 'beruf_add_buttons_shortcode' );
?>
