<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->


	<head>
		<meta charset="utf-8">

		<?php // Google Chrome Frame for IE ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php if (is_front_page()) { bloginfo('name'); } else { wp_title(''); } ?></title>

		<?php // mobile meta ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->

		<?php // icons & favicons http://www.favicon-generator.org/ ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php
			$inline_css = file_get_contents(get_template_directory_uri() . '/library/min/main.min.css');
			if($inline_css) {
			echo '<style>' . $inline_css . '</style>';
			}
		?>

	</head>

	<body <?php body_class(); ?>>

		<nav class="overlay" id="overlay">
			<?php mobile_nav(); ?>
		</nav>

		<header class="header">

			<nav id="main-menu" role="navigation">
				<div class="navbar navbar-dark fixed-top Fixed"> <!-- navbar-expand-lg -->
					<div class="container">

						<a class="navbar-brand" href="<?php bloginfo( 'url' ) ?>" title="<?php bloginfo( 'name' ) ?>;" rel="homepage">
							<img class="d-none d-sm-inline-block" src="<?php echo get_template_directory_uri() . '/library/svg/logo.svg'; ?>"/>
							<img class="d-block d-sm-none" src="<?php echo get_template_directory_uri() . '/library/svg/logo-xs.svg'; ?>"/>
						</a>

						<!-- New Hamburger button -->
						<button id="mobile-menu-button" aria-label="Menu" type="button" class="navbar-toggler hamburger hamburger--squeeze">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>

						<div class="navbar-collapse collapse navbar-responsive-collapse justify-content-end">
							<?php main_nav(); ?>
						</div>
					</div>
				</div>
			</nav>
		</header>

		<div class="mmenu-body">
