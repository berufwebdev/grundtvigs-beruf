<?php get_header(); ?>
    <div class="container">
    		<div id="content" class="clearfix col-12">

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <?php
                    $tilmelding = get_field('tilmelding_link');
                    $skjul_knap = array(
                        'tilmeld' => get_field('skjul_tilmelding'),
                        'vilkaar' => get_field('skjul_vilkaar')
                    );
                    $tilmelding = get_field('tilmelding_link');
                    $tilmelding = get_field('tilmelding_link');
                    // Featured image.
                    $thumb_id = get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
                    $thumb_url = $thumb_url_array[0];

                    $button_tilmelding = get_field('tilmelding_knap_tekst');
                    if (!$button_tilmelding) {
                      $button_tilmelding = "Tilmeld dig dette kursus!";
                    }

                    $button_vilkar = get_field('vilkar_knap_tekst');
                    if (!$button_vilkar) {
                      $button_vilkar = "Vilkår ved tilmelding";
                    }
              ?>

                <div class="row row-eq height">
                        <div class="col-12 col-md-6 order-2 order-md-1 bg-primary padbox">
                            <h1>Sommerkursus: <?php the_title(); ?></h1>
                            <?php the_content(); ?>
                            <p style="line-height:0;">
                                <?php if(!$skjul_knap['tilmelding']) : ?>
                                <a href="<?php echo $tilmelding; ?>" target="_blank" class="btn-grundtvigs btn-block red text-secondary icon-hand-o-right iconbg-secondary text-center tilmeld">
                                    <span><?php echo $button_tilmelding; ?></span>
                                </a>
                                <?php endif; ?>
                                <?php if(!$skjul_knap['vilkaar']) : ?>
                                <a href="<?php echo get_permalink(253); ?>" class="btn-grundtvigs btn-block green text-secondary icon-hand-o-right iconbg-secondary text-center vilkaar">
                                    <span><?php echo "$button_vilkar"; ?></span>
                                </a>
                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="col-12 col-md-6 order-1 order-md-2 bg-secondary padbox img-center img-cover" style="min-height:300px;background-image:url(<?php echo $thumb_url; ?>);">

                        </div>
                </div>

                <?php echo get_template_part('library/parts/page','builder'); ?>

                <div class="row">
                    <div class="col-12 mb-3 mt-3">
                        <p>Se alle <a href="<?php echo get_post_type_archive_link('sommerkursus'); ?>"><u>sommerkurser</u></a></p>
                    </div>
                </div>

              <?php endwhile; endif; ?>

    		</div>
    </div>
<?php get_footer(); ?>
