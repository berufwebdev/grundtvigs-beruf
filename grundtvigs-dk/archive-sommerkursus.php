<?php get_header(); ?>

<?php

    $id = 156;
    $sommerkursus = get_post($id);
    $title      = get_the_title($id);
    $content    = $sommerkursus->post_content;
?>

<div class="container-fluid" id="content">


    <div class="container">
        <div class="col-12">

            <div class="row">
                <div class="col-12 bg-tertiary padbox topbox">
                    <h1><?php echo $title; ?></h1>
                    <?php echo $content; ?>
                </div>
            </div>

            <?php
            $args = array(
                'p'         => $id, // ID of a page, post, or custom type
                'post_type' => 'page'
            );

            $page_query = new WP_Query($args);

            if ($page_query->have_posts()) :
                while ($page_query->have_posts()) : $page_query->the_post();

                    echo get_template_part('library/parts/page','builder');

                endwhile;
            endif;

            wp_reset_postdata();

            ?>

        </div>
    </div>

</div>

<?php get_footer(); ?>
