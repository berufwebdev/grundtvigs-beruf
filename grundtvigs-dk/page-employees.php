<?php
/*
Template Name: Medarbejdere
*/
?>
<?php get_header(); ?>

<div class="container-fluid" id="content">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="container">
        <div class="col-12 bg-tertiary padbox topbox">
            <h1><?php the_title(); ?></h1>
            <h6><?php the_content(); ?></h6>
        </div>
    </div>

    <div class="container">

        <!-- ************************************************************ -->

    <?php
        $teachers = get_field_object('teachers');
        if( have_rows('teachers') ):
    ?>

        <div class="col-12 nopad teachers">
            <h2><?php echo $teachers['label']; ?></h2>
        </div>

        <?php

            $row_no = 1;

            while ( have_rows('teachers') ) : the_row();

                $condition     = $row_no % 2;
                if(!$condition) {
                    $contentbox = ' order-2 order-md-2';
                    $imagebox   = ' order-1 order-md-1';
                } else {
                    $contentbox = ' order-2 order-md-1';
                    $imagebox   = ' order-1 order-md-2';
                }

                $name           = get_sub_field('name');
                $email          = get_sub_field('email');
                $description    = get_sub_field('description');
                $content        = get_sub_field('content');
                $image          = get_sub_field('image');
                $image_alt      = get_sub_field('image_alt'); ?>


                    <div class="col-12 employee">
                        <div class="row">
                            <div class="col-12 col-md-6 padbox<?php echo $contentbox ?>">
                                <h3 class="text-uppercase"><?php echo $name; ?></h3>
                                <h4><?php echo $description; ?></h4>
                                <p><?php echo $content; ?></p>
                                <a href="mailto:<?php echo $email; ?>" target="" class="btn-grundtvigs blue text-primary icon-envelope-o iconbg-tertiary">
                                    <span>Send mig en mail</span>
                                </a>
                            </div>
                            <div class="col-12 col-md-6 flipbox<?php echo $imagebox ?>">
                                <div class="flipbox_front">
                                    <div class="img-bg" style="background-image:url(<?php echo $image; ?>)"></div>
                                </div>
                                <div class="flipbox_back">
                                    <div class="img-bg" style="background-image:url(<?php echo $image_alt; ?>)"></div>
                                </div>
                            </div>
                        </div>
                    </div>


        <?php
            $row_no++;
            endwhile;
        endif;
        ?>

        <!-- ************************************************************ -->


        <?php
            $personale = get_field_object('personale');
            if( have_rows('personale') ):
        ?>

        <div class="col-12 nopad">
            <h2 class="personale"><?php echo $personale['label']; ?></h2>
        </div>

        <div class="row" style="margin:0;">
        <?php

            while ( have_rows('personale') ) : the_row();

                $name           = get_sub_field('name');
                $email          = get_sub_field('email');
                $title          = get_sub_field('title');
                $image          = get_sub_field('image');
                $image_alt      = get_sub_field('image_alt'); ?>

                <div class="col-12 col-md-6 employee">
                    <div class="row">
                        <div class="col-12 flipbox">
                            <div class="flipbox_front">
                                <div class="img-bg" style="background-image:url(<?php echo $image['url']; ?>)"></div>
                            </div>
                            <div class="flipbox_back">
                                <div class="img-bg" style="background-image:url(<?php echo $image_alt['url']; ?>)"></div>
                            </div>
                        </div>
                        <div class="col-12 padbox">
                            <h3><?php echo $name; ?></h3>
                            <p><?php echo $title; ?></p>
                            <a href="mailto:<?php echo $email; ?>"class="btn-grundtvigs blue text-primary icon-envelope-o iconbg-tertiary">
                                <span>Send mig en mail</span>
                            </a>
                        </div>
                    </div>
                </div>

                <?php
                    endwhile;
                endif;
                ?>

        </div>

        <!-- ************************************************************ -->

        <?php
            $bestyrelse = get_field_object('bestyrelse');
            if( have_rows('bestyrelse') ):
        ?>

        <div class="col-12 nopad">
            <h2 class="personale"><?php echo $bestyrelse['label']; ?></h2>
        </div>

        <div class="row bestyrelse" style="margin:0;">

            <?php
                while ( have_rows('bestyrelse') ) : the_row();

                    $name           = get_sub_field('name');
                    $description    = get_sub_field('description');
                    $image          = get_sub_field('image');
                    $image_alt      = get_sub_field('image_alt'); ?>

                <div class="col-12 col-md-6 employee">
                    <div class="row">
                        <div class="col-12 flipbox">
                            <div class="flipbox_front">
                                <div class="img-bg" style="background-image:url(<?php echo $image['url']; ?>)"></div>
                            </div>
                            <div class="flipbox_back">
                                <div class="img-bg" style="background-image:url(<?php echo $image_alt['url']; ?>)"></div>
                            </div>
                        </div>
                        <!-- <div class="col-12 img">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid">
                        </div> -->
                        <div class="col-12 padbox">
                            <h3><?php echo $name; ?></h3>
                            <p><?php echo $description; ?></p>
                        </div>
                    </div>
                </div>

            <?php
                endwhile;
            endif;
            ?>

        </div>


    </div>

    <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
