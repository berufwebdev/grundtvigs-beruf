<?php
	// Kontaktoplysninger
	$tlf 			= get_field('telefon','options');
	$tlf_url 		= str_replace(' ', '', $tlf);
	$email	 		= get_field('email','options');
	$address 		= get_field('adresse','options');
	// SoMe
	$ig 		= get_field('instagram','options');
	$yt 		= get_field('youtube','options');
	$fb 		= get_field('facebook','options');
?>

            <footer id="footer" class="clearfix">
				<div class="container">
					<div class="row">

						<div class="col-12 some">
							<p>
								<?php if($ig) : ?>
								<a href="<?php echo $ig; ?>" target="_blank" title="Gå til Grundtvigs Højskole på YouTube">
									<i class="fa fa-2x fa-instagram"></i>
								</a>
								<?php endif; ?>
								<?php if($yt) : ?>
								<a href="<?php echo $yt; ?>" target="_blank" title="Gå til Grundtvigs Højskole på YouTube">
									<i class="fa fa-2x fa-youtube"></i>
								</a>
								<?php endif; ?>
								<?php if($yt) : ?>
								<a href="<?php echo $fb; ?>" target="_blank" title="Gå til Grundtvigs Højskole på Facebook">
									<i class="fa fa-2x fa-facebook-square"></i>
								</a>
								<?php endif; ?>
							</p>
						</div>

						<div class="col-12 col-md-4 contact">
							<p>
								<?php echo $address ?><br>
								<a class="text-underline" href="mailto:<?php echo $email ?>"><?php echo $email ?></a><br>
								<a class="phone" href="tel:<?php echo $tlf_url ?>">Tlf. nr: <?php echo $tlf ?></a><br>
							</p>
							<a href="//hojskolerne.dk" target="_blank" class="mr-auto d-block">
								<img class="img-fluid" width="300px" src="<?php echo get_template_directory_uri() . '/library/images/' ?>/hojskolerne.png" alt="Sjællands ældste højskole - tæt på København">
							</a>

						</div>
						<div class="col-12 col-md-8">
							<a href="https://www.google.com/maps?saddr=My+Location&daddr=<?php echo $address ?>">
								<img src="<?php echo get_template_directory_uri() . '/library/images/' ?>/kort_footer.png" alt="" class="img-fluid">
							</a>
						</div>

					</div> <!-- end .row -->
				</div>
            </footer> <!-- end footer -->
        </div>  <!-- mmenu end -->
        <?php wp_footer(); ?>
    </body>

</html>
