<?php
/*
Template Name: Kontakt
*/
?>
<?php get_header(); ?>

<div class="container-fluid" id="content">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="container">
        <div class="col-12 bg-tertiary padbox topbox">
            <h1><?php the_title(); ?></h1>
            <h6><?php the_content(); ?></h6>
        </div>
    </div>

    <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
