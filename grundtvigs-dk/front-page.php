<?php
/*
Template Name: Forsiden
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="container-fluid" id="content">

    <div class="container">

        <div class="col-12">

            <div class="row mb_20">
                    <div class="col-12 col-md-6 col-lg-7 col-xl-8 topcontent">
                        <h1><?php bloginfo('sitename'); ?></h1>
                        <?php $toplink = get_field('top_link'); ?>
                        <p>
                            <a href="<?php echo $toplink['url']; ?>" target="<?php echo $toplink['target']; ?>" class="btn-grundtvigs blue text-primary icon-hand-o-right iconbg-primary">
                                <span><?php echo $toplink['title']; ?></span>
                            </a>
                        </p>
                    </div>
                    <div class="col-12 col-md-6 col-lg-5 col-xl-4 topslider nopad">
                        <?php

                        $images     = get_field('topslider');
                        $size       = 'large';
                        $attr       = array(
                            'class' => 'img-fluid',
                        );

                        if( $images ):
                            foreach( $images as $image ): ?>
                                    <div>
                                    	<?php echo wp_get_attachment_image( $image['ID'], $size, false, $attr ); ?>
                                    </div>
                        <?php
                            endforeach;
                        endif; ?>

                    </div>
            </div>

            <div class="row row-eq-height">

                    <div class="col-12 col-md-4 bg-tertiary padbox">
                        <?php
                            $b1_content = get_field('box_1_content');
                            $b1_link    = get_field('box_1_link');
                        ?>
                        <h3><?php echo $b1_content; ?></h3>
                        <a href="<?php echo $b1_link['url']; ?>" target="<?php echo $b1_link['target']; ?>" class="btn-grundtvigs blue text-tertiary icon-hand-o-right iconbg-primary">
                            <span><?php echo $b1_link['title']; ?></span>
                        </a>
                    </div>
                    <div class="col-12 col-md-4 bg-primary padbox">
                        <?php
                            $b2_content = get_field('box_2_content');
                            $b2_link    = get_field('box_2_link');
                        ?>
                        <h3><?php echo $b2_content; ?></h3>
                        <a href="<?php echo $b2_link['url']; ?>" target="<?php echo $b2_link['target']; ?>" class="btn-grundtvigs blue text-primary icon-hand-o-right iconbg-tertiary">
                            <span><?php echo $b2_link['title']; ?></span>
                        </a>
                    </div>
                    <div class="col-12 col-md-4 nopad sliderbox bg-secondary">

                        <?php
                        $today = date('Ymd');
                        $antal_begivenheder     = get_field('antal_begivenheder');
                        $args = array(
                        	'post_type'              => array( 'begivenhed' ),
                        	'post_status'            => array( 'publish' ),
                            'posts_per_page'         => $antal_begivenheder,
                        	//'posts_per_archive_page' => $antal_begivenheder,
                            'meta_key'			     => 'dato',
                        	'orderby'			     => 'meta_value',
                            'order'                  => 'ASC',
                            'meta_query'             => array(
                                                            array(
                                                                'key' 		=> 'dato',
                                                                'compare'	=> '>=',
                                                                'value' 	=> $today,
                                                            )
                                                        )
                        );
                        $cal_query = new WP_Query( $args );

                        if ( $cal_query->have_posts() ) : while ( $cal_query->have_posts() ) : $cal_query->the_post();

                            $thumb_id           = get_post_thumbnail_id();
                            $thumb_url_array    = wp_get_attachment_image_src($thumb_id, 'large', true);
                            $thumb_url          = $thumb_url_array[0];
                            $date               = get_field('dato',get_the_ID());
                            $title              = get_the_title();
                            $link               = get_the_permalink();
                        ?>

                        <a href="<?php echo $link; ?>">
                            <div class="slide" style="height:100%;background-image: url(<?php echo $thumb_url; ?>);">
                                <div class="slidepadbox">
                                    <p class="date"><?php echo $date; ?></p>
                                    <p class="info"><?php echo $title; ?></p>
                                </div>
                            </div>
                        </a>

                        <?php endwhile; endif; wp_reset_postdata(); ?>

                    </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-8 bg-secondary text-primary padbox">
                    <?php
                        $b4_content = get_field('box_4_content');
                        $b4_link    = get_field('box_4_link');
                    ?>
                    <h3><?php echo $b4_content; ?></h3>
                    <a href="<?php echo $b4_link['url']; ?>" target="<?php echo $b4_link['target']; ?>" class="btn-grundtvigs green text-secondary icon-hand-o-right iconbg-tertiary">
                        <span><?php echo $b4_link['title']; ?></span>
                    </a>
                </div>
                <div class="col-12 col-md-4 bg-tertiary padbox">
                    <?php
                        $b5_content = get_field('box_5_instagram');
                        $b5_link    = get_field('box_5_link')
                    ?>
                    <h3><?php echo $b5_content; ?></h3>
                    <a href="<?php echo $b5_link['url']; ?>" target="<?php echo $b5_link['target']; ?>" class="btn-grundtvigs blue text-tertiary icon-instagram iconbg-primary">
                        <span><?php echo $b5_link['title']; ?></span>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-4 flipbox">
                    <?php
                        $b6_content = get_field('box_6_content');
                        $b6_link    = get_field('box_6_link');
                        $b6_image   = get_field('box_6_image');
                    ?>
                    <div class="flipbox_front">
                        <div class="img-bg" style="background-image:url(<?php echo $b6_image['url']; ?>)"></div>
                    </div>
                    <div class="flipbox_back bg-secondary">
                        <div class="text-tertiary padbox">
                            <h3><?php echo $b6_content; ?></h3>
                            <a href="<?php echo $b6_link['url']; ?>" target="<?php echo $b6_link['target']; ?>" class="btn-grundtvigs red text-secondary icon-hand-o-right iconbg-primary">
                                <span><?php echo $b6_link['title']; ?></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 padbox bg-tertiary">
                    <?php
                        $b7_content = get_field('box_7_content');
                        $b7_link    = get_field('box_7_link');
                    ?>
                    <h3><?php echo $b7_content; ?></h3>
                    <a href="<?php echo $b7_link['url']; ?>" target="<?php echo $b7_link['target']; ?>" class="btn-grundtvigs blue text-tertiary icon-hand-o-right iconbg-primary">
                        <span><?php echo $b7_link['title']; ?></span>
                    </a>
                </div>
                <div class="col-12 col-md-4 padbox bg-secondary text-primary">
                    <?php
                        $b8_content = get_field('box_8_content');
                        $b8_link    = get_field('box_8_link');
                    ?>
                    <h3><?php echo $b8_content; ?></h3>
                    <a href="<?php echo $b8_link['url']; ?>" target="<?php echo $b8_link['target']; ?>" class="btn-grundtvigs green text-secondary icon-hand-o-right iconbg-tertiary">
                        <span><?php echo $b8_link['title']; ?></span>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-8 padbox bg-primary">
                    <?php if( have_rows('box_9_rep') ): ?>
                        <div class="sliderbox">
                        <?php while ( have_rows('box_9_rep') ) : the_row(); ?>

                            <div><p class="lead"><?php the_sub_field('lead'); ?></p></div>

                        <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <p>
                        <a href="<?php the_field('facebook','options') ?>" target="_blank" class="btn-grundtvigs blue text-primary icon-facebook-square iconbg-tertiary">
                            <span>Besøg os på facebook</span>
                        </a>
                    </p>
                </div>
                <div class="col-12 col-md-4 bg-primary flipbox">
                    <?php
                        $b10_content = get_field('box_10_content');
                        $b10_link    = get_field('box_10_link');
                        $b10_image   = get_field('box_10_image');
                    ?>
                    <div class="flipbox_front">
                        <div class="img-bg" style="background-image:url(<?php echo $b10_image['url']; ?>)"></div>
                    </div>
                    <div class="flipbox_back bg-primary">
                        <div class="padbox">
                            <h3><?php echo $b10_content; ?></h3>
                            <a href="<?php echo $b10_link['url']; ?>" target="<?php echo $b10_link['target']; ?>" class="btn-grundtvigs blue text-primary icon-hand-o-right iconbg-tertiary">
                                <span><?php echo $b10_link['title']; ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php
                    $yt_video = get_field('box_video');
                ?>
                <div class="col-12 bg-secondary videobox zoomed">
                    <div class="row">

                        <div id="ytplayer" data-url="<?php echo $yt_video; ?>"></div>
                        <p>
                            <a href="mailto:info@grundtvigs.dk" class="btn-grundtvigs red text-secondary icon-envelope-o iconbg-primary">
                                <span>Book en rundvisning</span>
                            </a>
                        </p>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
