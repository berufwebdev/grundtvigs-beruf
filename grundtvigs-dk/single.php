<?php get_header(); ?>
    <div class="container">
    		<div id="content" class="clearfix">

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix row'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                  <header class="article-header">
                    <div class="titlewrap clearfix">
                      <h1 class="single-title entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
                    </div>
                  </header>

                  <section class="entry-content single-content clearfix" itemprop="articleBody">

                    <?php the_content(); ?>

                  </section> <?php // end article section ?>

                </article>

                <?php

                $images     = get_field('galleri');
                $img_size   = 'large';
                $no_images  = sizeof($images);
                if($no_images == 1) {
                    $cols   = 12;
                } elseif($no_images < 3) {
                    $cols   = 6;
                } else {
                    $cols   = 4;
                }

                if( $images ): ?>
                    <div class="row row-eq-height">
                        <?php foreach( $images as $image ): ?>
                            <div class="col-12 col-sm-<?php echo $cols; ?> mb-3">
                                <a href="#/" target="_blank" data-lity data-lity-target="<?php echo $image['url'] ?>">
                                    <div class="img-center img-cover" style="height:300px; background-image:url(<?php echo $image['sizes'][$img_size] ?>);"></div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?php if(is_singular('post')) : ?>
                    <div class="row">
                        <div class="col-12 pb-3 pt-3">
                            <p>Læs flere nyheder fra Grundtvigs <a href="<?php echo get_post_type_archive_link('post'); ?>"><u>her!</u></a></p>
                        </div>
                    </div>
                <?php elseif (is_singular('fag')) : ?>
                    <div class="row d-none">
                        <div class="col-12 pb-3 pt-3">
                            <p>Se alle fag  <a href="<?php echo get_post_type_archive_link('fag'); ?>"><u>her!</u></a></p>
                        </div>
                    </div>
                <?php endif; ?>

              <?php endwhile; endif; ?>

    		</div>
    </div>
<?php get_footer(); ?>
