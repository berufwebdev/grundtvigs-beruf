<?php get_header(); ?>
    <div class="container-fluid" id="content">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="container">
            <div class="col-12">

                <div class="row">
                    <div class="col-12 bg-tertiary padbox topbox">
                        <h1><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="container">
            <div class="col-12">

                <?php echo get_template_part('library/parts/page','builder'); ?>

            </div>
        </div>

    <?php endwhile; endif; ?>

    </div>
<?php get_footer(); ?>
