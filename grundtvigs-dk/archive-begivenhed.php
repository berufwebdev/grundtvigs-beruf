<?php get_header(); ?>

<div class="container-fluid" id="content">

    <div class="container">
        <div class="col-12">

            <div class="row">
                <div class="col-12 bg-tertiary padbox topbox">
                    <h1>Kalender</h1>
                    <h6>En central del af Grundtvigs Højskole er at få indblik, indsigt og information udefra, derfor inviterer vi ofte mange forskellige foredragsholdere, musikere og andre inspirerende mennesker forbi vores højskole. Følg med her.</h6>
                </div>
            </div>

            <div class="row">
                <div class="col-12 nopad pt-3 pb-3">
                    <a href="<?php echo get_permalink(101); ?>" target="" class="btn-grundtvigs blue text-primary icon-hand-o-left iconbg-tertiary">
                        <span>Se tidligere begivenheder</span>
                    </a>
                </div>
            </div>

            <div class="col-12 nopad">
                <?php $index = 0; ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php
                    if($index % 2) {
                        $paragraph  = ' order-2 order-md-1';
                        $image      = ' order-1 order-md-2';
                    } else {
                        $paragraph  = ' order-2 order-md-2';
                        $image      = ' order-1 order-md-1';
                    }
                    $image_src  = get_the_post_thumbnail_url(get_the_ID(), 'medium-large');
                    $date_format= 'j. F';
                    $acf_date   = strtotime(get_field('dato', false, false));
                    $date       = date_i18n($date_format, $acf_date);
                ?>
                    <div class="row row-eq height nyhed">
                        <div class="col-12 col-md-6 bg-secondary text-primary content<?php echo $paragraph; ?>">
                            <h2><?php echo $date ?></h2>
                            <h3><a href="<?php the_permalink(); ?>" class="text-left text-primary"><?php the_title() ?></a></h3>
                            <p><?php the_excerpt(); ?></p>
                            <p>
                                <a href="<?php the_permalink(); ?>" class="btn-grundtvigs green text-secondary icon-hand-o-right iconbg-tertiary">
                                    <span>Læs mere</span>
                                </a>
                            </p>
                        </div>
                    <a href="<?php the_permalink(); ?>">
                        <div class="col-12 col-md-6 bg-secondary image img-center img-cover<?php echo $image; ?>" style="background-image:url(<?php echo $image_src; ?>)">
                    </a>
                        </div>
                    </div>
                <?php $index++; ?>
                <?php endwhile; endif; ?>

            </div>

        </div>
    </div>

</div>

<?php get_footer(); ?>
