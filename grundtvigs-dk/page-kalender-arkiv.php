<?php
/*
Template Name: Kalender arkiv
*/
?>
<?php get_header(); ?>

<div class="container-fluid" id="content">

<?php

    $today = date('Ymd');
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $args = array (
        'posts_per_page'    => 20,
        'paged'             => $paged,
        'post_type'         => 'begivenhed',
        'orderby'           => 'dato',
        'order'             => 'DESC',
        'meta_query'        => array(
        		array(
        	        'key'		=> 'dato',
        	        'compare'	=> '=>',
        	        'value'		=> $today,
        	    )
        ),
    );

    $query = new WP_Query( $args );
?>

    <div class="container">
        <div class="col-12">

            <div class="row">
                <div class="col-12 nopad pt-3 pb-3">
                    <a href="<?php echo get_post_type_archive_link('begivenhed') ?>" class="btn-grundtvigs blue text-primary icon-hand-o-left iconbg-tertiary">
                        <span>Tilbage</span>
                    </a>
                </div>
            </div>


            <div class="col-12 nopad">
                <?php $index = 0; ?>
                <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                <?php
                    if($index % 2) {
                        $paragraph  = ' order-2 order-md-1';
                        $image      = ' order-1 order-md-2';
                    } else {
                        $paragraph  = ' order-2 order-md-2';
                        $image      = ' order-1 order-md-1';
                    }
                    $image_src  = get_the_post_thumbnail_url(get_the_ID(), 'medium-large');
                    $date_format= 'j. F';
                    $acf_date   = strtotime(get_field('dato', false, false));
                    $date       = date_i18n($date_format, $acf_date);
                ?>
                    <div class="row row-eq height nyhed">
                        <div class="col-12 col-md-6 bg-secondary text-primary content<?php echo $paragraph; ?>">
                            <h2><?php echo $date; ?></h2>
                            <h3><a href="<?php the_permalink(); ?>" class="text-left text-primary"><?php the_title() ?></a></h3>
                            <p><?php the_excerpt(); ?></p>
                            <p>
                                <a href="<?php the_permalink(); ?>" class="btn-grundtvigs green text-secondary icon-hand-o-right iconbg-tertiary">
                                    <span>Læs mere</span>
                                </a>
                            </p>
                        </div>
                    <a href="<?php the_permalink(); ?>">
                        <div class="col-12 col-md-6 bg-secondary image img-center img-cover<?php echo $image; ?>" style="background-image:url(<?php echo $image_src; ?>)">
                    </a>
                        </div>
                    </div>
                <?php $index++; ?>
                <?php endwhile; ?>

                <div class="nav-previous alignleft"><?php echo get_previous_posts_link( '<i class="fa fa-hand-o-left"></i> Nyere begivenheder', $query->max_num_pages ); ?></div>
                <div class="nav-next alignright"><?php echo get_next_posts_link( 'Ældrere begivenheder <i class="fa fa-hand-o-right"></i>', $query->max_num_pages ); ?></div>

                <?php endif; ?>

            </div>

        </div>
    </div>

</div>

<?php get_footer(); ?>
