//gettting our node parts
const path = require('path')

//getting our gulp parts
const gulp = require('gulp'),
  sass = require('gulp-sass'),
  del = require('del'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  cssmin = require('gulp-cssmin'),
  runSequence = require('run-sequence'),
  babel = require('gulp-babel'),
  sourcemaps = require('gulp-sourcemaps'),
  sftp = require('gulp-sftp'),
  ftpAccess = require('./ftpAccess.json')


gulp.task('clean:scss', function() {
  del('./grundtvigs-dk/library/min/main.min.css')
  return del('./library/css')
})

gulp.task('clean:js', function() {
  del('./grundtvigs-dk/library/min/main.js')
  return del('./library/min/main.js.map')
})

gulp.task('clean', ['clean:scss', 'clean:js'], function() {

})

const gulpJSDependencies = [
  './build/js/libs/webfont.js',
  './build/js/fonts.js',
  './build/js/libs/jquery.fitvids.js',
  './build/js/libs/jquery.mmenu.min.all.js',
  './build/js/libs/modernizr.custom.min.js',
  './build/js/libs/popper.min.js',
  './build/js/libs/bootstrap.min.js',
  './build/js/libs/slick.min.js',
  './build/js/libs/cookie.js',
  './build/js/libs/instafeed.min.js',
  './build/js/libs/lity.min.js',
  './build/js/libs/jquery.awesome-cursor.min.js',

  './build/js/helper.js',
  './build/js/custom.js',
  './build/js/sliders.js',
  './build/js/youtube.js',
  './build/js/instafeed.js',
  './build/js/skema.js',

  './build/js/libs/angular.min.js',
  './build/js/libs/angular-ui.min.js'
]

gulp.task('javascript', function() {
  return gulp.src(gulpJSDependencies)
        .pipe(concat('main.min.js'))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./grundtvigs-dk/library/min'))
})

gulp.task('build-js', function() {
  return runSequence('clean:js', 'javascript', 'upload:js', function() {
  })
})

gulp.task('build-css', function() {
  return runSequence('clean:scss', 'sass', 'css', 'upload:css', function() {
  })
})

gulp.task('sass', function() {
  return gulp.src(['./build/scss/*.scss', './build/scss/other/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./grundtvigs-dk/library/css'))
})

gulp.task('css', function() {
  return gulp.src(['./grundtvigs-dk/library/css/**/*.css', './library/scss/**/*.css'])
        .pipe(concat('main.min.css'))
        .pipe(cssmin())
        .pipe(gulp.dest('./grundtvigs-dk/library/min'))
})

gulp.task('build', ['build-css', 'build-js'], function() {

})

gulp.task('watch:sass', function() {
  gulp.watch('./build/**/*.scss', ['build-css'])
})

gulp.task('watch:js', function() {
  gulp.watch('./grundtvigs-dk/library/**/*.js', ['build-js'])
})

gulp.task('watch:php', function() {
  var watcher = gulp.watch('./**/*.php')
  watcher.on('change', function(event) {
    var filePath = path.relative(__dirname, event.path)
    var remotePath = filePath.split(" d/")

    if (ftpAccess) {
      var accessFile = Object.assign({}, ftpAccess)
      var newPath = ''
      if (remotePath[0] !== filePath) {
        newPath = ''
        for (var i = 0; i < remotePath.length-1; i++) {
          newPath += '/' + remotePath[i]
        }
      }

      accessFile.remotePath = ftpAccess.remotePath
      accessFile.remotePath += newPath
      return gulp.src('./' + filePath) //path to the compiled files
          .pipe(sftp(accessFile))
    } else {
      return console.log('No ftpAccess file. Terminates upload')
    }
  })
})

gulp.task('watch', ['watch:sass', 'watch:js', 'watch:php'], function() {
  console.log('Ready to watch')
})

gulp.task('upload:min', function () {
    if (ftpAccess) {
      var accessFile = Object.assign({}, ftpAccess)
      accessFile.remotePath += '/library/min'
      return gulp.src('./grundtvigs-dk/library/min/*') //path to the compiled files
          .pipe(sftp(accessFile))
    } else {
      return console.log('No ftpAccess file. Terminates upload')
    }
})

gulp.task('upload:css', function () {
  if (ftpAccess) {
    var accessFile = Object.assign({}, ftpAccess)
    accessFile.remotePath += '/library/min' //path to the compiled css files
    return gulp.src('./grundtvigs-dk/library/min/*.css')
        .pipe(sftp(accessFile))
  } else {
    return console.log('No ftpAccess file. Terminates upload')
  }
})

gulp.task('upload:js', function () {
  if (ftpAccess) {
    var accessFile = Object.assign({}, ftpAccess)
    accessFile.remotePath += '/library/min' //path to the compiled js files
    return gulp.src(['./grundtvigs-dk/library/min/*.js','./grundtvigs-dk/library/min/*.map'])
        .pipe(sftp(accessFile))
  } else {
    return console.log('No ftpAccess file. Terminates upload')
  }
})

//Default is just watch
gulp.task('default', ['watch'], function() {

})
