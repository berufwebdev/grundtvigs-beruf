// how to toggle (skema template)
jQuery(document).ready(function($) {


    // Forklaring toggle button
    $('#how_to_toggle').on('click', function() {
        $(this).find('i.fa').toggleClass('fa-hand-o-right');
        $(this).find('i.fa').toggleClass('fa-hand-o-down');
    });

    var skema   = $('#skema');
    var courses = $('#courses');

    // Module buttons.
    $('#modul_1').on('click', function() {
        $(this).addClass('active');
        $('#modul_2').removeClass('active');
        $('#course_selector').text('Periode 1');
        courses.removeClass('module_2');
        courses.addClass('module_1');
        skema.removeClass('module_2');
        skema.addClass('module_1');
    });
    $('#modul_2').on('click', function() {
        $(this).addClass('active');
        $('#modul_1').removeClass('active');
        $('#course_selector').text('Periode 2');
        courses.removeClass('module_1');
        courses.addClass('module_2');
        skema.removeClass('module_1');
        skema.addClass('module_2');
    });

    $('.choosecourse').on('click', function() {
        var element = $(this);
        //var icon = element.find('i');
        var dataId = element.attr('data-id');
        var modul = element.attr('data-module');
        var blok = element.attr('data-block');
        var tekst = element.attr('data-text');
        // FAG A
        if(modul == undefined) {
            if(blok == 'block_a') {
                var kursus = $('#skema td.time.block_a');
                var inner = kursus.find('span');
                inner.text(tekst);
                kursus.addClass('chosen');
                inner.removeClass('default');
            }
        } else {
            var kursus = $('#skema td.time.' + blok);
            var inner = kursus.find('span.'+modul);
            inner.text(tekst);
            kursus.addClass('chosen');
            inner.removeClass('default');
        }
    });

    // Nustil knappen
    $('#reset').on('click', function() {
        var element = $(this);
        var aFag = $('td.time.block_a span');
        var b1Fag = $('td.time.block_b span.module_1');
        var b2Fag = $('td.time.block_b span.module_2');
        var c1Fag = $('td.time.block_c span.module_1');
        var c2Fag = $('td.time.block_c span.module_2');
        var d1Fag = $('td.time.block_d span.module_1');
        var d2Fag = $('td.time.block_d span.module_2');
        var e1Fag = $('td.time.block_e span.module_1');
        var e2Fag = $('td.time.block_e span.module_2');
        aFag.addClass('default');
        b1Fag.addClass('default');
        b2Fag.addClass('default');
        c1Fag.addClass('default');
        c2Fag.addClass('default');
        d1Fag.addClass('default');
        d2Fag.addClass('default');
        e1Fag.addClass('default');
        e2Fag.addClass('default');
        aFag.text('A');
        b1Fag.text('B');
        b2Fag.text('B');
        c1Fag.text('C');
        c2Fag.text('C');
        d1Fag.text('D');
        d2Fag.text('D');
        e1Fag.text('E');
        e2Fag.text('E');

    });

});
