jQuery(document).ready(function($) {

    $('.sliderbox').slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000
    });

    $('.topslider').slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 0,
        touchMove: false
    });

});
