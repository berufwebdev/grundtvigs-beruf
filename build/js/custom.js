// as the page loads, call these scripts
jQuery(document).ready(function($) {

    //getting viewport width
    var responsive_viewport = $(window).width();

    // if is below 481px
    if (responsive_viewport < 481) { }

    //if is larger than 481px
    if (responsive_viewport > 481) { }

    // if is above or equal to 768px
    if (responsive_viewport >= 768) {

        // load gravatars
        $('.comment img[data-gravatar]').each(function(){
            $(this).attr('src',$(this).attr('data-gravatar'));
        });

    }

    // off the bat large screen actions
    if (responsive_viewport > 1030) { }

});

// Add a span for each checkbox and radiobutton in formidable.
jQuery(document).ready(function($) {

    $('.with_frm_style .frm_checkbox input[type=checkbox], .with_frm_style .frm_radio input[type=radio]').each(function() {
        $(this).after('<span>');
    });
    // $('.with_frm_style .frm_radio input[type=radio]').after('<span>');

});

// flipbox
jQuery(document).ready(function($) {

    $('.flipbox').on('hover', function() {
        $(this).addClass('flipped');
    });
    $('.flipbox').mouseleave(function() {
        $(this).removeClass('flipped');
    });

});

// Look for .hamburger
var hamburger       = document.querySelector('button.hamburger');
var mobileMenu      = document.querySelector('#overlay');
var navBarBrand     = document.querySelector('.navbar-brand');
var docBody         = document.querySelector('.mmenu-body');
hamburger.addEventListener("click", function() {

    hamburger.classList.toggle('is-active');
    mobileMenu.classList.toggle('nav-active');
    document.body.classList.toggle('wm-nav-open');
    docBody.classList.toggle('d-none');

});

// Custom menu js
jQuery(document).ready(function($) {

    $('#overlay ul .menu-item-has-children').on('click', function() {
        $(this).find('.sub-menu').toggleClass('open');
        $(this).find('a').toggleClass('open');
    });

});
