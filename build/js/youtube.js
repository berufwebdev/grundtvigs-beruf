// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
var ytContainer = document.getElementById('ytplayer');
if(ytContainer) {
    var ytURL = ytContainer.getAttribute('data-url');
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('ytplayer', {
            playerVars: {
                showinfo: 0,
                controls: 0,
                loop: 1,
                fs: 0, // Setting this parameter to 0 prevents the fullscreen button from displaying in the player.
                modestbranding: 1,
                rel: 0,
                playlist: ytURL
            },
            events: {
                'onReady': onPlayerReadyMuted
            },
            height: '100%',
            width: '100%',
            videoId: ytURL
        });
    }
}
// 4. The API will call this function when the video player is ready.
function onPlayerReadyMuted(event) {
    player.setPlaybackRate(1);
    event.target.playVideo();
    event.target.setVolume(0);
}
function onPlayerReady(event) {
    player.setPlaybackRate(1);
    event.target.playVideo();
}
/*
    Add the functionality of enabling sound on click
    along with changing the cursor icon.
*/
jQuery.fn.awesomeCursor.defaults.color = 'white';
jQuery.fn.awesomeCursor.defaults.size = 32;
jQuery(document).ready(function($) {

    $('.videobox').awesomeCursor('volume-up');

    $('.videobox').on('click',function(event){
        var vol = player.getVolume();
        if(vol > 0) {
            player.setVolume(0);
            $('.videobox').awesomeCursor('volume-up');
        } else {
            player.setVolume(100);
            $('.videobox').awesomeCursor('volume-off');
        }
    });

});
