/************************************************************************

    MMenu initiation.

    http://mmenu.frebsite.nl/documentation/custom-build.html



jQuery(document).ready(function($) {

	$("#mobile-menu").mmenu({
        // Options
        "offCanvas": {
            "position": "top",
            "zposition": "top"
        }
  	});
    console.log('MMenu initiated.');

	var API = $("#mobile-menu").data( "mmenu" );
	$("#mobile-menu-button").click(function() {
	 	API.open();
	});
});
*************************************************************************/
