var igContainer = document.getElementById('instafeed');
if(igContainer) {
	var userFeed = new Instafeed({
		after: function() {
			// console.log('Instafeed initiated.');
		},

		get: 'user',
		sortBy: 'most-recent',
		userId: '488640502',
		accessToken: '488640502.6abddf7.5d384402348342e88c3230bb79ddbacf',
		resolution: 'standard_resolution',	
		template: '<div class="col-12 col-md-4 text-center insta_post"><a href="{{link}}" target="_blank"><img class="insta_img img-fluid" src="{{image}}"/><div class="insta_content"><p>{{likes}} <i class="fa fa-heart"></i> {{comments}} <i class="fa fa-comment"></i></p></div></a></div>',
		limit: 18
	});

	userFeed.run();
}
